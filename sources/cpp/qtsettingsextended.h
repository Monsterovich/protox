#ifndef QTSETTINGSEXTENDED_H
#define QTSETTINGSEXTENDED_H

#include "common.h"

class QSettingsExtended : public QSettings
{
	Q_OBJECT
public:
	QSettingsExtended(const QString &fileName);
	QVariant valueDefault(const QString &key);
private:
	QVariantMap default_values;
};

#endif // QTSETTINGSEXTENDED_H
