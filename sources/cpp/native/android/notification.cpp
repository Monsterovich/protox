/*
 * The following licenses are related only to this file.
 *
 * Copyright (c) 2021 Nikolay Borodin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Copyright (C) 2021 Siddesh Pillai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "notification.h"

#include "sources/cpp/native/android/qandroidjniobjecttools.h"

void QtNotification::show(const QVariant &notificationParameters)
{
	QVariantMap parameters = notificationParameters.toMap();
	QString caption = parameters.value("caption").toString();
	QString title = parameters.value("title").toString();
	int id = parameters.value("id").toInt();
	int type = parameters.value("type").toInt();
	QVariantMap additionalParameters = parameters.value("parameters").toMap();

	QAndroidJniObject javaCaption = QAndroidJniObject::fromString(caption);
	QAndroidJniObject javaTitle = QAndroidJniObject::fromString(title);
	QAndroidJniObject javaParameters = QAndroidJniObjectTools::fromVariantMap(additionalParameters);

	QAndroidJniObject::callStaticMethod<void>("org/protox/notifications/Notifications",
											  "show",
											  "(Ljava/lang/String;Ljava/lang/String;IILjava/util/HashMap;)V",
											  javaTitle.object(),
											  javaCaption.object(),
											  (jint)id,
											  (jint)type,
											  javaParameters.object());
}

void QtNotification::cancel(const QVariant &notificationParameters)
{
	QVariantMap parameters = notificationParameters.toMap();
	int id = parameters.value("id").toInt();
	int type = parameters.value("type").toInt();
	QVariantMap additionalParameters = parameters.value("parameters").toMap();
	QAndroidJniObject javaParameters = QAndroidJniObjectTools::fromVariantMap(additionalParameters);

	QAndroidJniObject::callStaticMethod<void>("org/protox/notifications/Notifications",
											  "cancel",
											  "(IILjava/util/HashMap;)V",
											  (jint)type,
											  (jint)id,
											  javaParameters.object());

}

void QtNotification::cancelAll()
{
	QAndroidJniObject::callStaticMethod<void>("org/protox/notifications/Notifications",
											  "cancelAll");
}

int QtNotification::getNotificationId(bool cancel)
{
	return QtAndroid::androidActivity().callMethod<jint>("getNotificationId", "(Z)I", cancel);
}

void QtNotification::declareQML()
{
	qmlRegisterType<QtNotification>("QtNotification", 1, 0, "Notification");
}
